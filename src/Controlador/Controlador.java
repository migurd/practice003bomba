
package Controlador;
// Uses
import Modelo.Bomba;
import Modelo.Gasolina;
import Vista.dlgBomba;
// Actions
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
// View
import javax.swing.JOptionPane;
import javax.swing.JFrame;

public class Controlador implements ActionListener {

    private Bomba bomba;
    private dlgBomba vista;
    
    // Constructor starts the objects and add the action listeners to the btns
    public Controlador(Bomba bomba, dlgBomba vista) {
        this.bomba = bomba;
        this.vista = vista;
        
        vista.btnIniciarBomba.addActionListener(this);
        vista.btnRegistrar.addActionListener(this);
        vista.btnDetenerBomba.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {

        // When you press iniciar bomba is gonna start everything
        if(e.getSource() == vista.btnIniciarBomba) {
            
            // You can't work with the model through view, it's better if the method is unused
            // but we call it anyway to say we're trying to use it
            
            // Save data
            try {
                if(Integer.parseInt(vista.txtNumBomba.getText()) < 0)
                    throw new IllegalArgumentException("La ID no puede ser negativa");
                //else if(Float.parseFloat(vista.txtPrecioGasolina.getText()) < 0)
                //    throw new IllegalArgumentException("El precio no puede ser negativo");              
                // The ID is the position plus 1
                bomba.iniciarBomba(Integer.parseInt(vista.txtNumBomba.getText()), Integer.parseInt(vista.txtCapacidadBomba.getText()), 0,
                                new Gasolina((vista.comboBoxTipoGasolina.getSelectedIndex()+1), 
                                Float.parseFloat(vista.txtPrecioGasolina.getText()), 
                                vista.comboBoxTipoGasolina.getSelectedItem().toString()));
                //System.out.println((vista.comboBoxTipoGasolina.getSelectedIndex()+1));
            }
            // Errores relacionados con números
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, ex.getMessage());
                return;
            }
            // Negativo
            catch(IllegalArgumentException ex2) {
                JOptionPane.showMessageDialog(vista, ex2.getMessage());
                return;
            }
            // Errores más amplios
            catch(Exception ex3) {
                JOptionPane.showMessageDialog(vista, ex3.getMessage());
                return;
            }
            
            JOptionPane.showMessageDialog(vista, "Se inició exitosamente la bomba");

            // bomb config get locked
            vista.btnIniciarBomba.setEnabled(false);
            vista.txtNumBomba.setEnabled(false);
            vista.comboBoxTipoGasolina.setEnabled(false);
            vista.sliderCapacidadBomba.setEnabled(false);
            
            // sell options get unlocked
            vista.txtGasolinaAVender.setEnabled(true);
            vista.btnRegistrar.setEnabled(true);
            
        }
        
        if(e.getSource() == vista.btnDetenerBomba) {
            
            int option = JOptionPane.showConfirmDialog(vista, "¿Seguro que quiere reiniciar la bomba?", "Confirmación de cerrar bomba", JOptionPane.YES_NO_OPTION);
            if(option != JOptionPane.YES_OPTION)
                return;
            
            limpiar();
            
            // bomb config get unlocked
            vista.btnIniciarBomba.setEnabled(true);
            vista.txtNumBomba.setEnabled(true);
            vista.comboBoxTipoGasolina.setEnabled(true);
            vista.sliderCapacidadBomba.setEnabled(true);
            
            // sell options get locked
            vista.txtGasolinaAVender.setEnabled(false);
            vista.btnRegistrar.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnRegistrar) {
            // We save the import for later
            float importe = 0;
            try {
                if(Integer.parseInt(vista.txtGasolinaAVender.getText()) < 0)
                    throw new IllegalArgumentException("No se puede registrar una cantidad negativa");
                importe = bomba.venderGasolina(Integer.parseInt(vista.txtGasolinaAVender.getText()));
            }
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex2) {
                JOptionPane.showMessageDialog(vista, ex2.getMessage());
                return;
            }
            
            // No enough founds
            if(importe == 0) {
                JOptionPane.showMessageDialog(vista, "No hay fondos suficientes");
                return;
            }
           
            // Enough founds
            JOptionPane.showMessageDialog(vista, "Se realizó la venta");
            
            // Update data after selling
            // --> Contador de ventas, sumar uno
            vista.txtContadorVentas.setText(Integer.toString(Integer.parseInt(vista.txtContadorVentas.getText())+1));
            // --> Contador de Acumulador de Litros, litros vendidos
            // vista.txtContadorVentas.setText(Float.toString(bomba.getAcumuladorDeLitros()));
            // --> Se resta del original
            vista.txtCapacidadBomba.setText(vista.txtCapacidadBomba.getText());
            // --> Restar gasolina vendida
            vista.txtCapacidadBomba.setText(Integer.toString(Integer.parseInt(vista.txtCapacidadBomba.getText()) - Integer.parseInt(vista.txtGasolinaAVender.getText())));
            
            // Set slider value
            vista.sliderCapacidadBomba.setValue(Integer.parseInt(vista.txtCapacidadBomba.getText()));
            
            // Update costs and total sells texts
            vista.txtCosto.setText(Float.toString(importe));
            vista.txtTotalVentas.setText(Float.toString(bomba.getVentasTotales()));
        }
        
        if(e.getSource() == vista.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(vista, "¿Seguro que quieres salir?", "Confirmación de salida", JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
       
    }
    
    public static void main(String[] args) {
        Bomba bomba = new Bomba();
        Gasolina gas = new Gasolina();
        dlgBomba vista = new dlgBomba(new JFrame(), true);
        
        Controlador contra = new Controlador(bomba, vista);
        contra.iniciarVista();
    }
    
    public void iniciarVista() {
        vista.setTitle("Bomba");
        vista.setSize(500, 680);
        vista.setVisible(true);
    }
    
    public void limpiar() {
        // Resets values
        vista.txtNumBomba.setText("");
        //vista.txtPrecioGasolina.setText("");
        vista.comboBoxTipoGasolina.setSelectedIndex(0);
        vista.txtContadorVentas.setText("0");
        vista.txtCapacidadBomba.setText("100");
        vista.sliderCapacidadBomba.setValue(100);
        vista.txtGasolinaAVender.setText("");
        vista.txtCosto.setText("");
        vista.txtTotalVentas.setText("");
    }
}
