
package Modelo;

/**
 *
 * @author quier
 */
public class Bomba {
    private int numBomba;
    private int capacidadBomba;
    private int acumuladorDeLitros;
    private Gasolina gas;
    
    // Constructors
    public Bomba() {
        numBomba = 0;
        capacidadBomba = 0;
        acumuladorDeLitros = 0;
        gas = new Gasolina();
    }
    
    public Bomba(Bomba otro) {
        this.numBomba = otro.numBomba;
        this.capacidadBomba = otro.capacidadBomba;
        this.acumuladorDeLitros = otro.acumuladorDeLitros;
        this.gas = otro.gas;
    }
    
    public Bomba(int numBomba, int capacidadBomba, int acumuladorDeLitros, Gasolina gas) {
        this.numBomba = numBomba;
        this.capacidadBomba = capacidadBomba;
        this.acumuladorDeLitros = acumuladorDeLitros;
        this.gas = gas;
    }

    // Setters and getters
    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public int getCapacidadBomba() {
        return capacidadBomba;
    }

    public void setCapacidadBomba(int capacidadBomba) {
        this.capacidadBomba = capacidadBomba;
    }

    public int getAcumuladorDeLitros() {
        return acumuladorDeLitros;
    }

    public void setAcumuladorDeLitros(int acumuladorDeLitros) {
        this.acumuladorDeLitros = acumuladorDeLitros;
    }

    public Gasolina getGasolina() {
        return gas;
    }

    public void setGasolina(Gasolina gas) {
        this.gas = gas;
    }
    
    // Logic
    // iniciarBomba terminó siendo como un constructor XD
    public void iniciarBomba(int numBomba, int capacidadBomba, int acumuladorDeLitros, Gasolina gas) {
        this.numBomba = numBomba;
        this.capacidadBomba = capacidadBomba;
        this.acumuladorDeLitros = acumuladorDeLitros;
        this.gas = gas;
    }
    
    public int getInventarioGasolina() {
        return this.capacidadBomba - this.acumuladorDeLitros;
    }
    
    public float venderGasolina(int gasolinaAVender) {
        // if the client wants more gasoline than we have
        // we cannot sell, so we sell 0
        if(gasolinaAVender > getInventarioGasolina())
            return 0;
        
        // Acumulador de litros
        this.acumuladorDeLitros += gasolinaAVender;
        
        // Returns import
        return (float) gasolinaAVender * gas.getPrecioGasolina();
        
    }
    
    public float getVentasTotales() {
        return this.acumuladorDeLitros * gas.getPrecioGasolina();
    }
    
}
