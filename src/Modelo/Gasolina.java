
package Modelo;

/**
 *
 * @author quier
 */
public class Gasolina {
    private int idGasolina;
    private float precioGasolina;
    private String tipoGasolina;
    
    public Gasolina() {
        idGasolina = 0;
        precioGasolina = 0;
        tipoGasolina = "";
    }
    
    public Gasolina(Gasolina otro) {
        this.idGasolina = otro.idGasolina;
        this.precioGasolina = otro.precioGasolina;
        this.tipoGasolina = otro.tipoGasolina;
    }
    
    public Gasolina(int idGasolina, float precioGasolina, String tipoGasolina) {
        this.idGasolina = idGasolina;
        this.precioGasolina = precioGasolina;
        this.tipoGasolina = tipoGasolina;
    }

    public int getIdGasolina() {
        return idGasolina;
    }

    public void setIdGasolina(int idGasolina) {
        this.idGasolina = idGasolina;
    }

    public float getPrecioGasolina() {
        return precioGasolina;
    }

    public void setPrecioGasolina(float precioGasolina) {
        this.precioGasolina = precioGasolina;
    }

    public String getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(String tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }
    
    
    
}
